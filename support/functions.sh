#!/bin/bash

# Collection of helper functions used in demos.
#

function print_docs()
{
  echo "To use this installation script you have to provide one argument"
  echo "indicating how you want to install the Perses server. You have the"
  echo "option to install a container image or build it from source:"
	echo
	echo "   $ ./init.sh {podman|source}"
	echo
	echo "Both methods are validated by the install scripts."
	echo
}


function validate_build_mode()
{
  local mode=$1  # passed arg to this function.

  if [[ $mode == "podman" ]]; then
    echo "Installing container image..."
    echo
    MODE="podman"
  elif [[ $mode == "source" ]]; then
    echo "Installing from source..."
    echo
    MODE="source"
  else
    # bad arg passed.
    print_docs
    echo
    exit
  fi
}


function unpack_perses_project() 
{
  echo "Unpacking Perses project into target directory:"
  command mkdir -p ./target

  if [ $? -ne 0 ]; then
      echo
  		echo "Error occurred during 'mkdir' command..."
  		echo
  		exit;
    fi

  command unzip ./installs/"${PERSES_SRC}" -d ./target 1> /dev/null

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred during 'unzip' command..."
		echo
		exit;
  fi

  echo "Perses project unpacked successfully into target directory!"
  echo
}


function setup_perses_server_examples()
{
  local mode=$1  # passed arg to this function.

  echo "Logging in to Perses instance..."
  echo
  command ./bin/percli login http://localhost:8080

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred during 'percli login' to perses container..."
		echo
		exit;
  fi

  sleep 3;

  echo "Applying the demo projects setup..."
  echo
  if [[ $mode == "podman" ]]; then
    command ./bin/percli apply -f workshop-project.json
  else
    # source build location.
    command ./bin/percli apply -f ../../support/workshop-project.json
  fi

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred during 'percli apply workshop-project' command..."
		echo
		exit;
  fi

  sleep 3;

  echo
  echo "Applying the dashboard setup..."
  echo
  if [[ $mode == "podman" ]]; then
    command ./bin/percli apply -f workshop-myfirstdashboard.json
  else
    # source build location.
    command ./bin/percli apply -f ../../support/workshop-myfirstdashboard.json
  fi

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred with 'percli apply workshop-myfirstdashboard' command..."
		echo
		exit;
  fi

  sleep 3;

  echo
  echo "Applying the variable setup..."
  echo
  if [[ $mode == "podman" ]]; then
    command ./bin/percli apply -f workshop-variables-instance.json
  else
    # source build location.
    command ./bin/percli apply -f ../../support/workshop-variables-instance.json
  fi

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred with 'percli apply workshop-variables-instance' command..."
		echo
		exit;
  fi

  sleep 3;

  echo
  echo "Applying the demo datasources..."
  echo
  if [[ $mode == "podman" ]]; then
    command ./bin/percli apply -f workshop-datasource-dashboard3000.json
  else
    # source build location.
    command ./bin/percli apply -f ../../support/workshop-datasource-dashboard3000.json
  fi

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred running the 'percli apply workshop-datasource-dashboard3000' command..."
		echo
		exit;
  fi

  sleep 3;

  if [[ $mode == "podman" ]]; then
    command ./bin/percli apply -f workshop-datasource-nodeexporter9100.json
  else
    # source build location.
    command ./bin/percli apply -f ../../support/workshop-datasource-nodeexporter9100.json
  fi

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred running the 'percli apply workshop-datasource-nodeexporter9100' command..."
		echo
		exit;
  fi

  sleep 3;

if [[ $mode == "podman" ]]; then
    command ./bin/percli apply -f workshop-datasource-prometheus8080.json
  else
    # source build location.
    command ./bin/percli apply -f ../../support/workshop-datasource-prometheus8080.json
  fi

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred running the 'percli apply workshop-datasource-prometheus8080' command..."
		echo
		exit;
  fi
}


function install_perses_in_container()
{
  # Check the  podman installation.
  echo "Checking if Podman is installed..."
  echo
  command -v podman --version -v  >/dev/null 2>&1 || { echo >&2 "Podman is required but not installed yet... download and install: https://podman.io/getting-started/installation"; exit; }


  echo "Starting the perses container image..."
  echo
  command podman run --name perses -d -p 127.0.0.1:8080:8080 persesdev/perses:"${IMG_VERSION}"

  if [ $? -ne 0 ]; then
		echo
    echo "Cleaning up any perses image that might still be running..."
    echo
    command podman container stop perses >/dev/null 2>&1
    command podman container rm perses >/dev/null 2>&1

    echo "Starting fresh perses container image..."
    echo
    command podman run --name perses -d -p 127.0.0.1:8080:8080 persesdev/perses:"${IMG_VERSION}"

    if [ $? -ne 0 ]; then
      echo
      if [[ $(uname) == "Darwin" ]]; then
        echo "Error occurred during 'podman run' starting perses container... make"
        echo "sure you have started the Podman machine as follows and rerun this"
        echo "installation script again:"
        echo
        echo "     $ podman machine start "
        echo
        exit;
      fi

      echo "Error occurred during 'podman run' starting perses container... make sure your Linux podman installation"
      echo "is initialized and working correctly before trying this installation script again."
      exit;
    fi
  fi

  echo
  echo "Waiting for container to start..."
  echo
  sleep 5;

  echo "Setting up the Perses examples..."
  echo
  command cd ./support

  if [ $? -ne 0 ]; then
      echo
  		echo "Error occurred during 'cd' to support command..."
  		echo
  		exit;
    fi

  setup_perses_server_examples $MODE
}


function install_perses_from_source()
{
  # Check for GO installation.
  command -v go version -v >/dev/null 2>&1 || { echo >&2 "Go language is required but not installed yet... download and install here: https://go.dev/doc/install"; exit; }

  # Check for NodeJS installation.
  command -v node --version -v >/dev/null 2>&1 || { echo >&2 "NodeJS is required but not installed yet... download and install here: https://nodejs.org/en/download"; exit; }

  # Check for npm installation.
  command -v npm --version -v >/dev/null 2>&1 || { echo >&2 "Npm is required but not installed yet... download and install for your OS (using brew, rpm, etc): https://www.npmjs.com"; exit; }

  # Remove the old Perses installation, if it exists.
  if [ -x "$PERSES_HOME" ]; then
    echo "  - removing existing installation directory..."
    echo
    rm -rf "$PERSES_HOME"
  fi

  # just to be sure that Perses is not running from a previous source install.
  command killall perses >/dev/null 2>&1

  # Version check for Go.
  echo "Checking for Go version needed to use Perses:"
  maj_version=$(go version | cut -d" " -f3 | cut -d "." -f1 | cut -d "o" -f2)
  min_version=$(go version | cut -d" " -f3 | cut -d "." -f2)

  if [ "${maj_version}" -ge "${MAJ_GO_VERSION}" ]; then
    echo "Go major version is good..."
    if [ "${min_version}" -ge "${MIN_GO_VERSION}" ]; then
      echo "Go minor version is good..."
      echo "Your Go version ${maj_version}.${min_version} accepted!"
      echo
    fi
  else
    echo "Your Go version must be ${GO_VERSION} or higher..."
    exit;
  fi

  # Version check for Node.
  echo "Checking for Node version needed to use Perses:"
  maj_version=$(node --version | cut -d"." -f1 | cut -d "v" -f2)

  if [ "${maj_version}" -ge "${NODE_VERSION}" ]; then
    echo "Node major version is good..."
    echo "Your Node version ${maj_version} accepted!"
    echo
  else
    echo "Your Node version must be ${NODE_VERSION} or higher..."
    exit;
  fi

  # Version check for NPM.
  echo "Checking for NPM version needed to use Perses:"
  maj_version=$(npm --version | cut -d"." -f1)
  echo "NPM version: ${maj_version}"

  if [ "${maj_version}" -ge "${NPM_VERSION}" ]; then
    echo "NPM major version is good..."
    echo "Your NPM version ${maj_version} accepted!"
    echo
  else
    echo "Your NPM version must be ${NPM_VERSION} or higher..."
    exit;
  fi

  unpack_perses_project

  echo "Let's move to the Perses project home directory..."
  command cd "${PERSES_HOME}"

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred during 'cd' to perses home command..."
		echo
		exit;
  fi

  echo
  echo "Building the Perses server (this takes a bit, so wait for it to complete):"
  echo
  command make build

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred with 'make build' command..."
		echo
		exit;
  fi

  echo "Perses server built successfully!"
  echo

  echo "Starting the perses server..."
  echo
  nohup command ./bin/perses -config ./dev/config.yaml >/dev/null 2>&1 &

  if [ $? -ne 0 ]; then
    echo
		echo "Error occurred during starting perses server..."
		echo
		exit;
  fi

  echo "Waiting for server to start..."
  echo
  sleep 5;


  echo "Setting up example workshop project..."
  echo

  setup_perses_server_examples source
}
