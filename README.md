Perses Easy Install
===================
This demo is to install Perses project for dashboards and visualization on your local machine from the supported
released version. It delivers a ready to use Perses installation. The installation scripts check for and validate
prerequisites automatically.

There are two options for installation detailed below.

Install in a container (Podman)
-------------------------------
This is an installation using the provided Perses container image. You will
run this container on a virtual machine provided by Podman.

**Prerequisites:** Podman 4.x+ with your podman machine started.

1. [Download and unzip this demo.](https://gitlab.com/o11y-workshops/perses-install-demo/-/archive/main/perses-install-demo-main.zip)

2. Run 'init.sh' with the correct argument: 

``` 
   $ podman machine init
   $ ./init.sh podman
```

3. The Perses container image is now running and pre-loaded with demo examples,
   connect in your browser::

```
   http://localhost:8080
```

You will find a WorkshopProject and if you open that, two example dashboards to
explore:

<img src="docs/demo-images/workshop-console.png" width="70%">

Install on your local machine
--------------------------------
This is an installation from the source code of the Perses project. You will
test, build, and deploy the Perses server locally on your machine.

**Prerequisites:** Go version 1.18+, NodeJS version 16+, npm version 8+

1. [Download and unzip this demo.](https://gitlab.com/o11y-workshops/perses-install-demo/-/archive/main/perses-install-demo-main.zip)

2. Run 'init.sh' or 'init.bat' file with the correct argument. Note 'init.bat' must be run with Administrative privileges.  

``` 
   $ ./init.sh source
   
   C: init.bat 
```

3. Perses is now running, connect to the Perses dashboards in your browser:

```
   http://localhost:8080
```

Should result in this view in your browser:

<img src="docs/demo-images/workshop-console.png" width="70%" alt="container-dashboard">


Getting started
---------------
Not sure how to get started with Perses dashboards and visualization? Try this  
<a href="https://o11y-workshops.gitlab.io/workshop-perses" target="_blank">online workshop:</a>

----------------------------------------------

<a href="https://o11y-workshops.gitlab.io/workshop-perses" target="_blank"><img src="docs/demo-images/workshop.png" width="70%"></a>

----------------------------------------------

Notes:
-----
If for any reason the installation breaks or you want a new Perses installation, just rerun the installation script to
reinstall the Perses project in the target directory. 


Supporting Articles
-------------------
- [O11y Guide - Your First Steps in Cloud Native Observability](https://www.schabell.org/2022/09/o11y-guide-your-first-steps-in-cloud-native-observability.html)


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v1.3 - Supporting Perses v0.41.1, installing in a container or from source, and windows install support.

- v1.2 - Supporting Perses v0.21.0, installing in a container or from source, and windows install support.

- v1.1 - Supporting Perses v0.21.0, installing in a container or from source.

- v1.0 - Supporting Perses v0.20.0, installing in a container or from source.

-----------------------------
![Perses Dashboard](docs/demo-images/dashboard-demo.png)
-----------------------------
