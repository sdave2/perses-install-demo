#!/bin/bash

DEMO="Perses Easy Install demo"
AUTHORS="Eric D. Schabell"
PROJECT="git@gitlab.com:o11y-workshops/perses-install-demo.git"
PERSES_VERSION=0.41.1
MODE="none"

# variables used globally in sourced functions.
export IMG_VERSION=v0.41.1
export PERSES_SRC=perses-${PERSES_VERSION}.zip
export MAJ_GO_VERSION=1
export MIN_GO_VERSION=18
export NODE_VERSION=16
export NPM_VERSION=8
export PERSES_HOME=./target/perses-${PERSES_VERSION}
export MODE="none"

# importing functions.
source ./support/functions.sh

# wipe screen.
clear

echo
echo "#############################################################"
echo "##                                                         ##"   
echo "##  Setting up the ${DEMO}                ##"
echo "##                                                         ##"   
echo "##           ####  ##### ####   #### #####  ####           ##"
echo "##           #   # #     #   # #     #     #               ##"
echo "##           ####  ###   ####   ###  ###    ###            ##"
echo "##           #     #     #  #      # #         #           ##"
echo "##           #     ##### #   # ####  ##### ####            ##"
echo "##                                                         ##"
echo "##                #####  ###   #### #   #                  ##"
echo "##                #     #   # #      # #                   ##"
echo "##                ###   #####  ###    #                    ##"
echo "##                #     #   #     #   #                    ##"
echo "##                ##### #   # ####    #                    ##"
echo "##                                                         ##"
echo "##        ##### #   #  #### #####  ###  #     #            ##"
echo "##          #   ##  # #       #   #   # #     #            ##"
echo "##          #   # # #  ###    #   ##### #     #            ##"
echo "##          #   #  ##     #   #   #   # #     #            ##"
echo "##        ##### #   # ####    #   #   # ##### #####        ##"
echo "##                                                         ##"   
echo "##  brought to you by ${AUTHORS}                     ##"
echo "##                                                         ##"   
echo "##  ${PROJECT}  ##"
echo "##                                                         ##"   
echo "#############################################################"
echo

echo "Checking the build mode arguments..."
echo
# check for build mode argument.
if [ $# -eq 1 ]; then
  # passing argument to function.
  validate_build_mode $1
elif [ $# -gt 1 ]; then
  # too many parameters passed, show docs.
  print_docs
  echo
  exit
else
  # no parameters passed, show docs.
	print_docs
	echo
	exit
fi

if [[ $MODE == "podman" ]]; then
  echo "Installing Perses container using image..."
  echo
  install_perses_in_container
elif [[ $MODE == "source" ]]; then
  echo "Installing Perses from the source project..."
  echo
  install_perses_from_source
else
  echo "The wrong mode was passed to install Perses: $MODE"
  echo
  exit;
fi

echo
echo "======================================================"
echo "=                                                    ="
echo "=  Install complete, get ready to rock Perses!       ="
echo "=                                                    ="
echo "=  The Perses dashboard can be opened at:            ="
echo "=                                                    ="
echo "=           http://localhost:8080                    ="
echo "=                                                    ="
echo "=  Getting started workshop available online:        ="
echo "=                                                    ="
echo "=  https://o11y-workshops.gitlab.io/workshop-perses  ="
echo "=                                                    ="

if [[ $MODE == "podman" ]]; then
  echo "=  Note: When finished using the Perses container,   ="
  echo "=  you can shut it down and come back later to       ="
  echo "=  restart it:                                       ="
  echo "=                                                    ="
  echo "=      $ podman container stop perses                ="
  echo "=                                                    ="
  echo "=  To remove the perses container and start over:    ="
  echo "=                                                    ="
  echo "=      $ podman container rm perses                  ="
  echo "=                                                    ="
  echo "=  Also, remember to shut down the virtual machine:  ="
  echo "=                                                    ="
  echo "=      $ podman machine stop                         ="
else
  echo "=  Note: When finished using the Perses server you   ="
  echo "=  can kill it from the command line with the        ="
  echo "=  following cmd to locate the process id (PID):     ="
  echo "=                                                    ="
  echo "=      $ ps aux | grep perses                        ="
  echo "=                                                    ="
  echo "=      $ kill [PID]                                  ="
fi

echo "=                                                    ="
echo "======================================================"
echo
