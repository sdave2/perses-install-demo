@ECHO OFF
setlocal

set PROJECT_HOME=%~dp0
set DEMO=Perses Easy Install demo
set AUTHORS=Eric D. Schabell
set PROJECT=git@gitlab.com:o11y-workshops/perses-install-demo.git
set PERSES_VERSION=0.41.1
set IMG_VERSION=v0.41.1
set PERSES_SRC=perses-%PERSES_VERSION%.zip
set MAJ_GO_VERSION=1
set MIN_GO_VERSION=18
set NODE_VERSION=16
set NPM_VERSION=8
set PERSES_HOME=.\target\perses-%PERSES_VERSION%

REM wipe screen.
cls

echo.
echo #############################################################
echo ##                                                         ##
echo ##  Setting up the %DEMO%                ##
echo ##                                                         ##
echo ##           ####  ##### ####   #### #####  ####           ##
echo ##           #   # #     #   # #     #     #               ##
echo ##           ####  ###   ####   ###  ###    ###            ##
echo ##           #     #     #  #      # #         #           ##
echo ##           #     ##### #   # ####  ##### ####            ##
echo ##                                                         ##
echo ##                #####  ###   #### #   #                  ##
echo ##                #     #   # #      # #                   ##
echo ##                ###   #####  ###    #                    ##
echo ##                #     #   #     #   #                    ##
echo ##                ##### #   # ####    #                    ##
echo ##                                                         ##
echo ##        ##### #   #  #### #####  ###  #     #            ##
echo ##          #   ##  # #       #   #   # #     #            ##
echo ##          #   # # #  ###    #   ##### #     #            ##
echo ##          #   #  ##     #   #   #   # #     #            ##
echo ##        ##### #   # ####    #   #   # ##### #####        ##
echo ##                                                         ##
echo ##  brought to you by %AUTHORS%                     ##
echo ##                                                         ##
echo ##  %PROJECT%  ##
echo ##                                                         ##
echo #############################################################
echo.

echo Installing Perses from the source project...
echo.

echo Checking if GO is installed...
echo.
call go version -v >nul 2>&1

if %ERRORLEVEL% EQU 0 GOTO :exitGoCheck
  echo Go language is required but not installed yet... download and install here: https://go.dev/doc/install
  echo.
  GOTO :EOF

:exitGoCheck

echo Checking if NodeJS is installed...
echo.
call node --version -v >nul 2>&1

if %ERRORLEVEL% EQU 0 GOTO :exitNodeCheck
  echo NodeJS is required but not installed yet... download and install here: https://nodejs.org/en/download
  echo.
  GOTO :EOF

:exitNodeCheck

echo Checking if NPM is installed...
echo.
call npm --version -v >nul 2>&1

if %ERRORLEVEL% EQU 0 GOTO :exitNpmCheck
  echo Npm is required but not installed yet... download and install for your OS (using brew, rpm, etc): https://www.npmjs.com
  echo.
  GOTO :EOF

:exitNpmCheck

REM remove the old Perses installation, if it exists.
call RMDIR %PERSES_HOME% 2>nul

REM TODO - add version check for GO.
REM TODO - add version check for NodeJS.
REM TODO - add version check for NPM.

REM unpacking the Perses source project.
set vbs="support\unzip.vbs"

echo Unpacking Perses project into target directory...
call mkdir target
call %vbs% "installs\%PERSES_SRC%" "target\"

if %ERRORLEVEL% EQU 0 GOTO :unzipSuccess
  echo There was a problem with unpacking the Perses project into target directory...
  echo.
  GOTO :EOF

:unzipSuccess
echo Perses project unpacked successfully into target directory...
echo.
echo Let's run the Perses project tests (wait for it to complete):
call CD %PERSES_HOME%

if %ERRORLEVEL% EQU 0 GOTO :readyToBuild
  echo Error occurred during 'cd' to Perses home command...
  echo.
  GOTO :EOF

:readyToBuild
echo Building the Perses server (this takes a bit, so wait for it to complete):
echo.
call make build

if %ERRORLEVEL% EQU 0 GOTO :readyToStartServer
  echo Error occurred during 'make build' command...
  echo.
  GOTO :EOF

:readyToStartServer

echo Perses server built successfully...
echo.
echo Starting the Perses server (in a new terminal)...
echo.
call start /k bin\perses -config dev\config.yaml

if %ERRORLEVEL% EQU 0 GOTO :serverStarted
  echo Error occurred during starting Perses server...
  echo.
  GOTO :EOF

:serverStarted
echo Waiting for server to start...
echo.

timeout /t 5

REM produce the installation end report.
echo.
echo ======================================================
echo =                                                    =
echo =  Install complete, get ready to rock Perses!       =
echo =                                                    =
echo =  The Perses dashboard can be opened at:            =
echo =                                                    =
echo =           http://localhost:8080                    =
echo =                                                    =
echo =  Getting started workshop available online:        =
echo =                                                    =
echo =  https://o11y-workshops.gitlab.io/workshop-perses  =
echo =                                                    =
echo =  Note: When finished using the Perses server you   =
echo =  can kill it from the terminal it started in.      =
echo =  Look for the open terminal running Perses.        =
echo =                                                    =
echo ======================================================
echo.
GOTO :EOF
